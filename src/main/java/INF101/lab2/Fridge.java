package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    @Override
    public int totalSize() {
        return 20;
    }

    List<FridgeItem> fridgeContents = new ArrayList<>(totalSize());

    @Override
    public int nItemsInFridge() {
        return (fridgeContents.size());
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge()<totalSize()){
            fridgeContents.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeContents.contains(item)) {
            fridgeContents.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeContents.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredList = new ArrayList<>();
        for(FridgeItem item : fridgeContents){
            if (item.hasExpired()){
                expiredList.add(item);
            }
        }
        for(FridgeItem item : expiredList){
            takeOut(item);
        }
        return expiredList;
    }
}
